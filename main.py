# -*- coding: utf-8 -*-
"""
Created on Thu Oct 28 09:45:06 2021

"""

import J2735
import socket
import binascii

from os.path import join

event_state = {
     "unavailable": "N",
     "dark": "D",
     "stop-Then-Proceed": "T",
     "stop-And-Remain": "S",
     "pre-Movement": "M",
     "protected-Movement-Allowed": "P",
     "permissive-Movement-Allowed": "R",
     "protected-clearance": "C",
     "permited-clearance": "M",
     "caution-Conflicting-Traffic": "L"
     }


def retreive_data(socket, buffer_size=1024):
     try:
          data, _ = sock.recvfrom(1024)
          hex_spat_data = binascii.hexlify(data)
          utf_spat_data = hex_spat_data.decode('utf-8')
          
          decoded_spat = J2735.DSRC.MessageFrame
          decoded_spat.from_uper(binascii.unhexlify(utf_spat_data))
          
          fin_dec_spact = decoded_spat()
          return fin_dec_spact
     except socket.error:
          print("UDP Connection Lost")
          return None

def retreive_ID(content, IDs):
     if content['value'][1]["intersections"][0]["id"]["id"] in IDs:
          return content['value'][1]["intersections"][0]["id"]["id"]
     else:
          return None

def retreive_signal_state(content):
     results = {}
     for item in content['value'][1]["intersections"][0]["states"]:
          signal_group = item["signalGroup"]
          for sub_item in item['state-time-speed']:
               results[signal_group] = sub_item["eventState"]
     return results

def verify_signal(signal, n_signal_groups):
     if len(signal) < n_signal_groups:
          for _ in range(n_signal_groups - len(signal)):
               signal += "X"
     return signal

def retreive_signal(results, n_signal_groups=8, verbose=False):
     final_output = ""
     for signal in sorted(results, reverse=False):
          if verbose:
               print(signal, results[signal])
          if results[signal] in event_state and len(final_output) < n_signal_groups:
               final_output += event_state[results[signal]]
     return verify_signal(final_output, n_signal_groups)

def write_date(IDs, output_file="result.txt", verbose=False):
     final_string = ""
     for ID, key in IDs.items():
          if verbose:
               print("ID: {} Key: {}".format(ID, key))
          final_string+=key
     with open(output_file, "w") as file:
          file.write(final_string)
          #print(final_string)
          # # # Move read cursor to the start of file.
            # file.seek(0)
            # # If file is not empty then append '\n'
            # data = file.read(100)
            # if len(data) > 0 :
                # file.write("\n")
            # # Append text at the end of file
            # file.write(final_string)
          
def write_log(IDs, log_file="result.txt", verbose=False):
     final_string = ""
     for ID, key in IDs.items():
          if verbose:
               print("ID: {} Key: {}".format(ID, key))
          final_string+=key
     with open(log_file, "a+") as file:
          # file.write(final_string)
          # print(final_string)
          # # Move read cursor to the start of file.
            file.seek(0)
            # If file is not empty then append '\n'
            data = file.read(100)
            if len(data) > 0 :
                file.write("\n")
            # Append text at the end of file
            file.write(final_string)

def connect(UDP_IP, UDP_PORT):
     sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
     sock.bind((UDP_IP_REC, UDP_PORT_REC))
     return sock

if __name__ == "__main__":
     source_directory = "input"
     target_directory = ""; target_file = "inputMAPSPaT.txt"
     output_file = join(target_directory, target_file)

     IDs = {1001: "XXXXXXXX", 1002: "XXXXXXXX", 1003: "XXXXXXXX", 1004: "XXXXXXXX", 1005: "XXXXXXXX"}
     n_signal_groups = 8; verbose=False
     
     UDP_IP_REC="172.31.11.131"; UDP_PORT_REC = 49152; UDP_BUFFER_SIZE = 1024

     sock = connect(UDP_IP_REC, UDP_PORT_REC)
     
     while True:
          try:
               content = retreive_data(sock, buffer_size=UDP_BUFFER_SIZE)
               if content:
                    ID = retreive_ID(content, IDs)
          
                    if verbose:
                         print("received ID: {}".format(ID))
          
                    if ID in IDs:
                         results = retreive_signal_state(content)
                         signal_keyword = retreive_signal(results, n_signal_groups, verbose=verbose)
                         
                         if signal_keyword:
                              IDs[ID] = signal_keyword
                              write_date(IDs, output_file, verbose=verbose)
                              log_file = = join(target_directory, "cumu_inputMAPSPaT.txt")
                              write_log(IDs, log_file, verbose=verbose)
               else:
                    print("Reconnecting..!")
                    sock = connect(UDP_IP_REC, UDP_PORT_REC)
          except Exception as e:
               print(str(e))

sock.close()
