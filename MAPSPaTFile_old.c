/* MAP SPaT parse and logic function  */
/* file input output added */
/* 20211016 wlf  */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/***************************************************************************/

/* function that takes vehicle location and returns laneID, laneMovement, and getReadyForGreen flag */
/*****>>>>>>>> Should be renamed when compliled with the rest of the server code <<<<<<<<<<<********/

void main ()
{
/* function variables 			*/
/* MAP data tables  */

/*****************************************************/
/* These tables are static and based on the MAP data samples from Berkeley */
/* demonstration boundaries  	*/
	double boundaryNorthLat = 374406180;
double boundaryNorthLon = -1221639050;
double boundarySouthLat = 374169150;
double boundarySouthLon = -1221481960;

/* intsection reference points - array with index = intersection ID  */
double referencePointX [] = {-1221617100, -1221601111, -1221546841, -1221517955, -1221491403};
double referencePointY [] = {  374396157,   374373351,   374325427,   374299919,   374177711};

/* arrays for intersection 1  - array with index = lane number */
double _1lanePositionX [] = { -1221625902, -1221625705,   0, -1221625353, -1221625573, -1221625791,   0,   0,   0,   0,   0, -1221629186, -1221628722, -1221628441, -1221628198, -1221627827 };
double _1lanePositionY [] = {   374397742,   374397457,   0,   374396042,   374395825,   374395583,   0,   0,   0,   0,   0,   374395766,   374396103,   374396291,   374396487,   374396652 };
int _1maneuver []       = {        'B',         'L', 'N',         'T',         'S',         'S', 'N', 'N', 'N', 'N', 'N',         'S',         'S',         'S',         'S',         'L' };      
int _1signalGroup []    = {          4,           4,   0,           6,           6,           6,   0,   0,   0,   0,   0,           2,           2,           1,           1,           5 };

/* arrays for intersection 2  - array with index = lane number */
double _2lanePositionX [] = {-1221599317, -1221599077, -1221598793,   0,   0,   0, -1221596872, -1221598733, -1221599160, -1221599537, -1221599939,   0,   0,   0,   0, -1221602714, -1221602539, -1221602767, -1221603035,   0,   0,   0, -1221604512,   -1221603441, -1221603134, -1221602801, -1221602492 };
double _2lanePositionY [] = {  374374399,   374374169,   374373934,   0,   0,   0,   374371730,   374372591,   374372468,   374372375,   374372247,   0,   0,   0,   0,   374371206,   374371770,   374371980,   374372214,   0,   0,   0,   374373533,     374373645,   374373832,   374374015,   374374204 };
int _2maneuver []       = {        'S',         'S',         'L', 'N', 'N', 'N',         'R',         'S',         'S',         'S',         'L', 'N', 'N', 'N', 'N',         'R',         'S',         'S',         'L', 'N', 'N', 'N',        'R',            'S',         'S',         'S',         'L' };      
int _2signalGroup []    = {          4,           4,           4,   0,   0,   0,           6,           6,           6,           6,           1,   0,   0,   0,   0,           8,           8,           8,           3,   0,   0,   0,          2,              2,           2,           2,           5 };

/* arrays for intersection 3  - array with index = lane number */
double _3lanePositionX[]  = {-1221545903, - 1221545599,   0, - 1221544754, - 1221545026, - 1221545295, - 1221545606,   0,   0,   0, - 1221548763, - 1221548486, - 1221548164, - 1221547862 };
double _3lanePositionY[]  = {  374327282,    374326820,   0,    374325402,    374325202,    374325011,    374324792,   0,   0,   0,    374325374,    374325585,    374325788,    374325998 };
int _3maneuver[]        = {        'R',         'L',  'N',        'R',         'S',         'S',              'S', 'N', 'N', 'N',         'S',          'S',          'S',           'L' };
int _3signalGroup[]	 = {          4,           4,    0,          6,           6,           6,                6,   0,   0,   0,           2,            2,            2,             5 };

/* arrays for intersection 4  - array with index = lane number */
double _4lanePositionX[]  = { -1221516125, - 1221515807,   0, - 1221515691, - 1221516055, - 1221516350, - 1221516677,   0,   0,   0, - 1221519339, - 1221519685,   0,   0, - 1221520227, - 1221519816, - 1221519413, - 1221519058 };
double _4lanePositionY[]  = {   374301392,    374301094,   0,    374299738,    374299541,    374299349,    374299161,   0,   0,   0,    374298490,    374298821,   0,   0,    374300485,    374300608,    374300734,    374300865 };
int _4maneuver[]        = {         'R',         'S',  'N',        'S',           'S',           'S',           'L', 'N', 'N', 'N',          'R',          'S', 'S', 'N',          'S',          'S',          'S',          'L' };
int _4signalGroup[]     = {           4,           4,    0,          6,            6,              6,             1,   0,   0,   0,            8,           8,    8,   0,            2,            2,            2,            5 };

/* arrays for intersection 5  - array with index = lane number */
double _5lanePositionX [] = {-1221491282,   0, -1221490535, -1221490761, -1221490972, -1221491205,   0,   0,   0, -1221493232, -1221493532,   0, -1221494541, -1221494260, -1221493986, -1221493721 };
double _5lanePositionY [] = {  374279358,   0,   374277567,   374277300,   374277074,   374276832,   0,   0,   0,   374275841,   374276100,   0,   374278166,   374278373,   374278532,   374278724 };
int _5maneuver []       = {         'B', 'N',         'R',         'S',          'S',        'L', 'N', 'N', 'N',         'R',         'S', 'N',         'S',         'S',         'S',         'L' };      
int _5signalGroup []    = {           4,   0,          6,           6,            6,           1,   0,   0,   0,           8,           8,   0,           2,           2,           2,           5 };

/*************************************************************/
/* Active variables used by the function each time it is called */

/* vehhicle location unit change */
double vehicleLocationCurrentLat = 37.439828;
double vehicleLocationCurrentLong = -122.162518;
double vehicleLocationPreviousLat = 37.439870;
double vehicleLocationPreviousLong = -122.161431;

/* 0 = out of range, 1, 2, 3, 4, 5 ... 28*/
int laneID = 0;
/* (leftRight = B, left = L , notUsed = N, straightRight = T, straight = S, right = R) */
char laneMovement = 'N';
/* (notUsed = N, red = R, yellow = Y, green =G) */
char signalColor;
/* (Unset = 0, Set = 1) */
char getReadyForGreen = '0';		/* 1 = Get Ready for Green Alert can be sent */

double vehicleLocationCurrentY;
double vehicleLocationCurrentX;
double vehicleLocationPreviousY;
double vehicleLocationPreviousX;

/* house keeping */
int intersectionID = 0;
/* travelDirection values {stopped = P, north = N, south = S) */
int travelDirection ='P';
double directionTest = 0; 
double deltaPosition = 0;
int counterFill = 0;
int counterCheck = 0;
int counterCheck1 = 0;
int signalGroup = 0;
double laneDistanceSmallest = 1000000;

/* intersection determination  */
double distance[5] = { 0, 0, 0, 0, 0 };

/* lane determination  */
double laneDistance[28] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

/**********************************************************************/
/* Coded versions of the behavior panels in the documentation .pdf */

/* console input interface commented out */

char str[50];

FILE *fp;

	      fp = fopen("lat_long.txt" , "r");

//	      printf("Current Latitude: ");   
//	      fgets(str, 11, stdin);
	      fgets(str, 12, fp);
	      printf("\n");
	      printf(str);
	      vehicleLocationCurrentLat = atof(str);
	      printf("Current Latitude: %f", vehicleLocationCurrentLat);
//	      printf("\n");
//	      printf("Current Longitude: ");
//	      fgets(str, 13, stdin);
	      fgets(str, 14, fp);
	      printf("\n");
	      printf(str);
	      vehicleLocationCurrentLong = atof(str);
	      printf("Current Lonitude: %f", vehicleLocationCurrentLong);
//	      printf("\n");
//	      printf("Current EventState: ");
	      
	      fclose(fp);
	      
	      fp = fopen("inputMAPSPaT.txt", "r");
	      
//	      fgets(str, 42, stdin);
	      fgets(str, 42, fp);
	      printf("\n");
	      printf(str);
	      printf("\n");
	      
	      fclose(fp);
	      
	      
		  /* EventState values {unavailable = N, dark = D, stop-Then=Proceed = T, stop-and-Remain = S, pre-Movement = M  protected-Movement-Allowed = P, permissive-Movement-Allowed = R, protected-Clearance = C, permited-clearance = M, caution-Conflicting-Traffic = L} */

	      int _1signalGroup1EventState = str[0];
	      int _1signalGroup2EventState = str[1];
	      int _1signalGroup3EventState = str[2];
	      int _1signalGroup4EventState = str[3];
	      int _1signalGroup5EventState = str[4];
	      int _1signalGroup6EventState = str[5];
	      int _1signalGroup7EventState = str[6];
	      int _1signalGroup8EventState = str[7];

	      int _2signalGroup1EventState = str[8];
	      int _2signalGroup2EventState = str[9];
	      int _2signalGroup3EventState = str[10];
	      int _2signalGroup4EventState = str[11];
	      int _2signalGroup5EventState = str[12];
	      int _2signalGroup6EventState = str[13];
	      int _2signalGroup7EventState = str[14];
	      int _2signalGroup8EventState = str[15];

	      int _3signalGroup1EventState = str[16];
	      int _3signalGroup2EventState = str[17];
	      int _3signalGroup3EventState = str[18];
	      int _3signalGroup4EventState = str[19];
	      int _3signalGroup5EventState = str[20];
	      int _3signalGroup6EventState = str[21];
	      int _3signalGroup7EventState = str[22];
	      int _3signalGroup8EventState = str[23];

	      int _4signalGroup1EventState = str[24];
	      int _4signalGroup2EventState = str[25];
	      int _4signalGroup3EventState = str[26];
	      int _4signalGroup4EventState = str[27];
	      int _4signalGroup5EventState = str[28];
	      int _4signalGroup6EventState = str[29];
	      int _4signalGroup7EventState = str[30];
	      int _4signalGroup8EventState = str[31];

	      int _5signalGroup1EventState = str[32];
	      int _5signalGroup2EventState = str[33];
	      int _5signalGroup3EventState = str[34];
	      int _5signalGroup4EventState = str[35];
	      int _5signalGroup5EventState = str[36];
	      int _5signalGroup6EventState = str[37];
	      int _5signalGroup7EventState = str[38];
	      int _5signalGroup8EventState = str[39];


/* Panel 1 - function entry */

//	P1:
	
		vehicleLocationCurrentX = vehicleLocationCurrentLong * 10000000;
		vehicleLocationCurrentY = vehicleLocationCurrentLat *10000000;
		vehicleLocationPreviousX = vehicleLocationPreviousLong * 10000000;
		vehicleLocationPreviousY = vehicleLocationPreviousLat * 10000000;
			
		if (vehicleLocationCurrentY > boundaryNorthLat)
		{
			signalColor = 'N';
			laneMovement = 'N';
			getReadyForGreen = 0;
			goto P5;
		}
	
		if (vehicleLocationCurrentY < boundarySouthLat)
		{
			signalColor = 'N';
			laneMovement = 'N';
			getReadyForGreen = 0;
			goto P5;
		}
	
		deltaPosition = sqrt((vehicleLocationCurrentX - vehicleLocationPreviousX)*(vehicleLocationCurrentX - vehicleLocationPreviousX) + (vehicleLocationCurrentY - vehicleLocationPreviousY)*(vehicleLocationCurrentY - vehicleLocationPreviousY));
		
		if (deltaPosition < 100)
		{
			travelDirection = 'P';
			counterFill = 0;
			counterCheck = 0;
			goto P2;
		}
		
		directionTest = vehicleLocationCurrentY - vehicleLocationPreviousY;
		
		if (directionTest > 0)
		{
			travelDirection = 'N';
			counterFill = 0;
			counterCheck = 0;
			goto P2;
		}
			
		travelDirection = 'S';
		counterFill = 0;
		counterCheck = 0;
		goto P2;
	
/* Panel 2 Determine intersection */

	P2:
	if (counterFill <= 4)
		{
		distance[counterFill] = sqrt((vehicleLocationCurrentX - referencePointX[counterFill])*(vehicleLocationCurrentX - referencePointX[counterFill]) + (vehicleLocationCurrentY - referencePointY[counterFill])*(vehicleLocationCurrentY - referencePointY[counterFill]));
			counterFill ++;
			goto P2;
		}
		if (counterCheck <= 4)
		{
			counterCheck1 = counterCheck + 1;
			if (distance[counterCheck] > distance[counterCheck1])
			{
				counterCheck ++;
				goto P2;
			}
		}
		intersectionID = counterCheck + 1;
		counterFill = 0;
		counterCheck = 0;
		laneID = 0;
		laneDistanceSmallest = 1000000;
		if (intersectionID == 1) goto P31;
		if (intersectionID == 2) goto P32;
		if (intersectionID == 3) goto P33;
		if (intersectionID == 4) goto P34;
    	if (intersectionID == 5) goto P35;


/* Panel 3-1  Determine lane, intersection 1 */

	P31:
	if (counterFill <= 15)
	{
		laneDistance[counterFill] = sqrt((vehicleLocationCurrentX - _1lanePositionX[counterFill])*(vehicleLocationCurrentX - _1lanePositionX[counterFill]) + (vehicleLocationCurrentY - _1lanePositionY[counterFill])*(vehicleLocationCurrentY - _1lanePositionY[counterFill]));
			counterFill ++;
			goto P31;
	}
	if (counterCheck <= 15)
		{

			if (laneDistance[counterCheck] < laneDistanceSmallest)
			{
				laneDistanceSmallest = laneDistance[counterCheck];
				laneID = counterCheck;
				counterCheck++;
				goto P31;
			}
			counterCheck++;
			goto P31;
		}

		laneMovement = _1maneuver[laneID];
		signalGroup = _1signalGroup[laneID];
		laneID = laneID + 1;
		counterFill = 0;
		counterCheck = 0;
		getReadyForGreen = '0';
		if (intersectionID == 1) goto P41;
		if (intersectionID == 1) goto P42;
		if (intersectionID == 3) goto P43;
		if (intersectionID == 4) goto P44;
		goto P45;




/* Panel 3-2  Determine lane, intersection 2 */

	P32:
	if (counterFill <= 26)
	{
		laneDistance[counterFill] = sqrt((vehicleLocationCurrentX - _2lanePositionX[counterFill])*(vehicleLocationCurrentX - _2lanePositionX[counterFill]) + (vehicleLocationCurrentY - _2lanePositionY[counterFill])*(vehicleLocationCurrentY - _2lanePositionY[counterFill]));
			counterFill ++;
			goto P32;
	}
	if (counterCheck <= 26) 
		{

			if (laneDistance[counterCheck] < laneDistanceSmallest)
			{
				laneDistanceSmallest = laneDistance[counterCheck];
				laneID = counterCheck;
				counterCheck++;
				goto P32;
			}
			counterCheck++;
			goto P32;
		}

		laneMovement = _2maneuver[laneID];
		signalGroup = _2signalGroup[laneID];
		laneID = laneID + 1;
		counterFill = 0;
		counterCheck = 0;
		getReadyForGreen = '0';
		if (intersectionID == 1) goto P41;
		if (intersectionID == 2) goto P42;
		if (intersectionID == 3) goto P43;
		if (intersectionID == 4) goto P44;
		goto P45;

/* Panel 3-3  Determine lane, intersection 3 */

	P33:
	if (counterFill <= 13)
	{
		laneDistance[counterFill] = sqrt((vehicleLocationCurrentX - _3lanePositionX[counterFill])*(vehicleLocationCurrentX - _3lanePositionX[counterFill]) + (vehicleLocationCurrentY - _3lanePositionY[counterFill])*(vehicleLocationCurrentY - _3lanePositionY[counterFill]));
			counterFill ++;
			goto P33;
	}
	if (counterCheck <= 13)
		{

			if (laneDistance[counterCheck] < laneDistanceSmallest)
			{
				laneDistanceSmallest = laneDistance[counterCheck];
				laneID = counterCheck;
				counterCheck++;
				goto P33;
			}
			counterCheck++;
			goto P33;
		}

		laneMovement = _3maneuver[laneID];
		signalGroup = _3signalGroup[laneID];
		laneID = laneID + 1;
		counterFill = 0;
		counterCheck = 0;
		getReadyForGreen = '0';
		if (intersectionID == 1) goto P41;
		if (intersectionID == 2) goto P42;
		if (intersectionID == 3) goto P43;
		if (intersectionID == 4) goto P44;
		goto P45;

/* Panel 3-4  Determine lane, intersection 4 */

	P34:
	if (counterFill <= 17)
	{
		laneDistance[counterFill] = sqrt((vehicleLocationCurrentX - _4lanePositionX[counterFill])*(vehicleLocationCurrentX - _4lanePositionX[counterFill]) + (vehicleLocationCurrentY - _4lanePositionY[counterFill])*(vehicleLocationCurrentY - _4lanePositionY[counterFill]));
			counterFill ++;
			goto P34;
	}
	if (counterCheck <= 17)
		{

			if (laneDistance[counterCheck] < laneDistanceSmallest)
			{
				laneDistanceSmallest = laneDistance[counterCheck];
				laneID = counterCheck;
				counterCheck++;
				goto P34;
			}
			counterCheck++;
			goto P34;
		}

		laneMovement = _4maneuver[laneID];
		signalGroup = _4signalGroup[laneID];
		laneID = laneID + 1;

		counterFill = 0;
		counterCheck = 0;
		getReadyForGreen = '0';
		if (intersectionID == 1) goto P41;
		if (intersectionID == 2) goto P42;
		if (intersectionID == 3) goto P43;
		if (intersectionID == 4) goto P44;
		goto P45;

/* Panel 3-5  5Determine lane, intersection 5 */

	P35:
	if (counterFill <= 15)
	{
		laneDistance[counterFill] = sqrt((vehicleLocationCurrentX - _5lanePositionX[counterFill])*(vehicleLocationCurrentX - _5lanePositionX[counterFill]) + (vehicleLocationCurrentY - _5lanePositionY[counterFill])*(vehicleLocationCurrentY - _5lanePositionY[counterFill]));
			counterFill ++;
			goto P35;
	}
	if (counterCheck <= 15)
		{

			if (laneDistance[counterCheck] < laneDistanceSmallest)
			{
				laneDistanceSmallest = laneDistance[counterCheck];
				laneID = counterCheck;
				counterCheck++;
				goto P35;
			}
			counterCheck++;
			goto P35;
		}

		laneMovement = _5maneuver[laneID];
		signalGroup = _5signalGroup[laneID];
		laneID = laneID + 1;
		counterFill = 0;
		counterCheck = 0;
		getReadyForGreen = '0';		
		if (intersectionID == 1) goto P41;
		if (intersectionID == 2) goto P42;
		if (intersectionID == 3) goto P43;
		if (intersectionID == 4) goto P44;
		goto P45;


/* Panel 4-1  – determine signal state, intersection 1 */ 	
	
	P41:
	if (signalGroup == 1)
	{
		_1signalGroup1EventState = 'N';
		if (_1signalGroup1EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_1signalGroup1EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_1signalGroup4EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;
	}
	
	if (signalGroup == 2)
	{
		if (_1signalGroup2EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_1signalGroup2EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_1signalGroup4EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;	
	}
	
	if (signalGroup == 3)
	{
		if (_1signalGroup3EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_1signalGroup3EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_1signalGroup2EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;			
	}
	
	if (signalGroup == 4)
	{
		if (_1signalGroup4EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_1signalGroup4EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_1signalGroup2EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;	
	}
	
	if (signalGroup == 5)
	{
		if (_1signalGroup5EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_1signalGroup5EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_1signalGroup8EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (signalGroup == 6)
	{
		if (_1signalGroup6EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_1signalGroup6EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_1signalGroup8EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (signalGroup == 7)
	{
		if (_1signalGroup7EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_1signalGroup7EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_1signalGroup6EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (_1signalGroup8EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_1signalGroup8EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_1signalGroup6EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	
		
/* Panel 4-2  – determine signal state, intersection 1 */ 

	P42:

		if (signalGroup == 1)
	{
		if (_2signalGroup1EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_2signalGroup1EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_2signalGroup4EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;
	}
	
	if (signalGroup == 2)
	{
		if (_2signalGroup1EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_2signalGroup2EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_2signalGroup4EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;	
	}
	
	if (signalGroup == 3)
	{
		if (_2signalGroup3EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_2signalGroup3EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_1signalGroup1EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;			
	}
	
	if (signalGroup == 4)
	{
		if (_2signalGroup4EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_2signalGroup4EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_2signalGroup1EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;	
	}
	
	if (signalGroup == 5)
	{
		if (_2signalGroup5EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_2signalGroup5EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_2signalGroup8EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (signalGroup == 6)
	{
		if (_2signalGroup6EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_2signalGroup6EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_2signalGroup8EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (signalGroup == 7)
	{
		if (_2signalGroup7EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_2signalGroup7EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_2signalGroup6EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (_2signalGroup8EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_2signalGroup8EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_2signalGroup6EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		


/* Panel 4-3  – determine signal state, intersection 3 */ 

	P43:
		if (signalGroup == 1)
	{
		if (_3signalGroup1EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_3signalGroup2EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_3signalGroup4EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;
	}
	
	if (signalGroup == 2)
	{
		if (_3signalGroup2EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_3signalGroup2EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_3signalGroup4EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;	
	}
	
	if (signalGroup == 3)
	{
		if (_3signalGroup3EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_3signalGroup3EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_3signalGroup2EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;			
	}
	
	if (signalGroup == 4)
	{
		if (_3signalGroup4EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_3signalGroup4EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_3signalGroup2EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;	
	}
	
	if (signalGroup == 5)
	{
		if (_3signalGroup5EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_3signalGroup5EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_3signalGroup8EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (signalGroup == 6)
	{
		if (_3signalGroup6EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_3signalGroup6EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_3signalGroup8EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (signalGroup == 7)
	{
		if (_3signalGroup7EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_3signalGroup7EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_3signalGroup6EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (_3signalGroup8EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_3signalGroup8EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_3signalGroup6EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		

	
/* Panel 4-4  – determine signal state, intersection 4 */ 

	P44:
		if (signalGroup == 1)
	{
		if (_4signalGroup1EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_4signalGroup2EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_4signalGroup4EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;
	}
	
	if (signalGroup == 2)
	{
		if (_4signalGroup1EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_4signalGroup2EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_4signalGroup4EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;	
	}
	
	if (signalGroup == 3)
	{
		if (_4signalGroup3EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_4signalGroup3EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_4signalGroup2EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;			
	}
	
	if (signalGroup == 4)
	{
		if (_4signalGroup4EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_4signalGroup4EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_4signalGroup2EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;	
	}
	
	if (signalGroup == 5)
	{
		if (_4signalGroup5EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_4signalGroup5EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_4signalGroup8EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (signalGroup == 6)
	{
		if (_4signalGroup6EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_4signalGroup6EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_4signalGroup8EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (signalGroup == 7)
	{
		if (_4signalGroup7EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_4signalGroup7EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_4signalGroup6EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (_4signalGroup8EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_4signalGroup8EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_4signalGroup6EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		



/* Panel 4-5  – determine signal state, intersection 5 */ 

	P45:
	if (signalGroup == 1)
	{
		if (_5signalGroup1EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_5signalGroup1EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_5signalGroup4EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;
	}
	
	if (signalGroup == 2)
	{
		if (_5signalGroup2EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_5signalGroup2EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_5signalGroup4EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;	
	}
	
	if (signalGroup == 3)
	{
		if (_5signalGroup3EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_5signalGroup3EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_5signalGroup2EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;			
	}
	
	if (signalGroup == 4)
	{
	
		if (_5signalGroup4EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_5signalGroup4EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_5signalGroup2EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;	
	}
	
	if (signalGroup == 5)
	{
		if (_5signalGroup5EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_5signalGroup5EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_5signalGroup8EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (signalGroup == 6)
	{
		if (_5signalGroup6EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_5signalGroup6EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_5signalGroup8EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (signalGroup == 7)
	{
		if (_5signalGroup7EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_5signalGroup7EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_5signalGroup6EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		
	}
	if (_5signalGroup8EventState == ('P' | 'R'))
		{
			signalColor = 'G';
			goto P5;
		}
		if (_5signalGroup8EventState == 'C')
		{
			signalColor = 'Y';
			goto P5;
		}
		laneMovement = 'N';
		signalColor = 'R';
		if (_5signalGroup6EventState == 'C')
		{
			getReadyForGreen = '1';
			goto P5;
		}
		getReadyForGreen = '0';
		goto P5;		




/* Panel 5 – Function exit */

	P5: 

/* console interface  commented out*/

		fp = fopen("outputMAPSPaT.txt" , "w");
		
		printf("Intersection ID: %d", intersectionID);
		printf("\n");
//		printf("Lane ID: %d", laneID);
//		printf("\n");
//		printf("Lane Movement : %c", laneMovement);
//		printf("\n");
//		printf("Signal Color: %c", signalColor);
//		printf("\n");
//		printf("Get Ready for Green flag: %d", getReadyForGreen);
//		printf("\n");
		
		fprintf(fp, "%c", laneMovement);
		fprintf(fp, "\n");
		fprintf(fp, "%c", signalColor);
		fprintf(fp, "\n");
		fprintf(fp, "%c", getReadyForGreen);
		fprintf(fp, "\n");
		
		fclose(fp);
		
		return;
}